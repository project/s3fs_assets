<?php

namespace Drupal\s3fs_assets\StreamWrapper;

use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\Url;
use Drupal\s3fs\StreamWrapper\S3fsStream;

/**
 * Defines a Drupal s3fs stream wrapper class for optimized assets(assets://).
 */
class AssetsS3fsStream extends S3fsStream {

  /**
   * {@inheritdoc}
   */
  public static function getType(): int {
    return StreamWrapperInterface::LOCAL_HIDDEN;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->t('Optimized assets files (s3fs)');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Public local optimized assets files served from Amazon S3.');
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    $target = str_replace('\\', '/', $this->streamWrapperManager::getTarget($this->uri));
    $path = explode('/', $target);

    $route_name = match ($path[0]) {
      'css' => 'system.css_asset',
      'js' => 'system.js_asset',
      default => NULL,
    };

    if ($route_name && !empty($path[1])) {
      return Url::fromRoute(
        $route_name,
        ['file_name' => $path[1]],
        ['absolute' => TRUE, 'path_processing' => FALSE]
      )->toString();
    }

    throw new \LogicException(sprintf('No suitable asset controller for file "%s".', $target));
  }

  /**
   * Gets the path that the wrapper is responsible for.
   *
   * @return string
   *   String specifying the path.
   */
  public function getDirectoryPath() {
    return Settings::get('file_assets_path', PublicStream::basePath());
  }

}
