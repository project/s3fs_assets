<?php

namespace Drupal\s3fs_assets;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies stream_wrapper.assets service.
 */
class S3fsAssetsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('stream_wrapper.assets')) {
      $container->getDefinition('stream_wrapper.assets')
        ->setClass('Drupal\s3fs_assets\StreamWrapper\AssetsS3fsStream');
    }
  }

}
